package ru.volnenko.se.command.system;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class HelpCommand extends AbstractCommand {

    List<AbstractCommand> commands;

    @Autowired
    private void setCommands(List<AbstractCommand> commands) {
        this.commands = commands;
    }

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        System.out.println(
                "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread.currentThread()
                        .getName());
        for (AbstractCommand command : commands) {
            System.out.println(command.command() + ": " + command.description());
        }
    }

    @EventListener(condition = "#event.name eq 'help'")
    public void listener(SimpleEvent event){
        execute();
    }

}
