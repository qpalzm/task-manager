package ru.volnenko.se.command.task;

import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class TaskRemoveCommand extends AbstractCommand {

    private Scanner scanner;

    @Autowired
    private void setScanner() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println(
                "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread.currentThread()
                        .getName());
        System.out.println("[REMOVING TASK]");
        System.out.println("Enter task order index:");
        String line = scanner.nextLine();
        final Integer orderIndex = getInteger(line);
        if (orderIndex == null) {
            System.out.println("Error! Incorrect order index...");
            System.out.println();
            return;
        }
        System.out.println("[OK]");
    }

    @EventListener(condition = "#event.name eq 'task-remove'")
    public void listener(SimpleEvent event) {
        execute();
    }

    private Integer getInteger(String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }

}
