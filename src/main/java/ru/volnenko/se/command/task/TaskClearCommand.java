package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class TaskClearCommand extends AbstractCommand {

    ITaskRepository taskRepository;

    @Autowired
    private void setTaskRepository(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public void execute() {
        taskRepository.clear();
        System.out.println("[ALL TASK REMOVED]");
    }

    @EventListener(condition = "#event.name eq 'task-clear'")
    public void listener(SimpleEvent event) {
        execute();
    }

}
