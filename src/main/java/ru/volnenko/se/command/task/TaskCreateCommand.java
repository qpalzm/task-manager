package ru.volnenko.se.command.task;

import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class TaskCreateCommand extends AbstractCommand {

    ITaskRepository taskRepository;
    private Scanner scanner;

    @Autowired
    private void setTaskRepository(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    private void setScanner() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }


    @Override
    public void execute() {
        System.out.println(
                "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread.currentThread()
                        .getName());
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = scanner.nextLine();
        taskRepository.createTask(name);
        System.out.println("[OK]");
        System.out.println();
    }

    @EventListener(condition = "#event.name eq 'task-create'")
    public void listener(SimpleEvent event) {
        execute();
    }

}
