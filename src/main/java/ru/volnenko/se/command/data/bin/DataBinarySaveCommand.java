package ru.volnenko.se.command.data.bin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.constant.DataConstant;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class DataBinarySaveCommand extends AbstractCommand {

    private static final Logger logger = LogManager.getLogger(DataBinarySaveCommand.class);

    ITaskService taskService;

    IProjectService projectService;

    @Autowired
    private void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Autowired
    private void setProjectService(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public String command() {
        return "data-bin-save";
    }

    @Override
    public String description() {
        return "Load data from binary file.";
    }


    @Override
    public void execute() {
        try {
            System.out.println(
                    "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread
                            .currentThread()
                            .getName());
            System.out.println("[DATA BINARY SAVE]");
            final Project[] projects = projectService.getListProject().toArray(new Project[] {});
            final Task[] tasks = taskService.getListTask().toArray(new Task[] {});

            final File file = new File(DataConstant.FILE_BINARY);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            executeObjectInputStream(file, projects, tasks);
            System.out.println("[OK]");
            System.out.println();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    private void executeObjectInputStream(File file,
                                          Project[] projects, Task[] tasks) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(projects);
            objectOutputStream.writeObject(tasks);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @EventListener(condition = "#event.name eq 'data-bin-save'")
    public void listener(SimpleEvent event) {
        execute();
    }

}
