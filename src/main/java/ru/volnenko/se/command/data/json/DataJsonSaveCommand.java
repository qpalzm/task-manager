package ru.volnenko.se.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.File;
import java.nio.file.Files;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.constant.DataConstant;
import ru.volnenko.se.entity.Domain;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class DataJsonSaveCommand extends AbstractCommand {

    private static final Logger logger = LogManager.getLogger(DataJsonSaveCommand.class);

    IDomainService domainService;

    @Autowired
    private void setDomainService(IDomainService domainService) {
        this.domainService = domainService;
    }

    @Override
    public String command() {
        return "data-json-save";
    }

    @Override
    public String description() {
        return "Save Domain to JSON.";
    }


    @Override
    public void execute() {
        try {
            System.out.println(
                    "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread
                            .currentThread()
                            .getName());
            System.out.println("[DATA JSON SAVE]");
            final Domain domain = new Domain();
            domainService.export(domain);
            final ObjectMapper objectMapper = new ObjectMapper();
            final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
            final String json = objectWriter.writeValueAsString(domain);
            final byte[] data = json.getBytes("UTF_8");
            final File file = new File(DataConstant.FILE_JSON);
            Files.write(file.toPath(), data);
            System.out.println("[OK]");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @EventListener(condition = "#event.name eq 'data-json-save'")
    public void listener(SimpleEvent event) {
        execute();
    }
}
