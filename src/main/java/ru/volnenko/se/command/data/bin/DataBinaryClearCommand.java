package ru.volnenko.se.command.data.bin;

import java.io.File;
import java.nio.file.Files;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.constant.DataConstant;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */

@Component
public class DataBinaryClearCommand extends AbstractCommand {

    private static final Logger logger = LogManager.getLogger(DataBinaryClearCommand.class);

    @Override
    public String command() {
        return "data-bin-clear";
    }

    @Override
    public String description() {
        return "Remove binary data.";
    }


    @Override
    public void execute() {
        try {
            System.out.println(
                    "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread
                            .currentThread()
                            .getName());
            final File file = new File(DataConstant.FILE_BINARY);
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @EventListener(condition = "#event.name eq 'data-bin-clear'")
    public void listener(SimpleEvent event) {
        execute();
    }

}
