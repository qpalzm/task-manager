package ru.volnenko.se.command.data.bin;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.constant.DataConstant;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class DataBinaryLoadCommand extends AbstractCommand {

    private static final Logger logger = LogManager.getLogger(DataBinaryLoadCommand.class);

    ITaskService taskService;

    IProjectService projectService;

    @Autowired
    private void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Autowired
    private void setProjectService(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public String command() {
        return "data-bin-load";
    }

    @Override
    public String description() {
        return "Save data to binary file.";
    }


    @Override
    public void execute() {
        try (FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY)) {
            System.out.println(
                    "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread
                            .currentThread()
                            .getName());
            System.out.println("[DATA BINARY LOAD]");
            executeObjectInputStream(fileInputStream);
            System.out.println("[OK]");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    private void executeObjectInputStream(final FileInputStream fileInputStream) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);) {
            loadProjects(objectInputStream.readObject());
            loadTasks(objectInputStream.readObject());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @EventListener(condition = "#event.name eq 'data-bin-load'")
    public void listener(SimpleEvent event) {
        execute();
    }

    private void loadProjects(final Object value) {
        if (!(value instanceof Project[])) {
            return;
        }
        final Project[] projects = (Project[]) value;
        projectService.load(projects);
    }

    private void loadTasks(final Object value) {
        if (!(value instanceof Task[])) {
            return;
        }
        final Task[] tasks = (Task[]) value;
        taskService.load(tasks);
    }

}
