package ru.volnenko.se.command.data.xml;

import java.io.File;
import java.nio.file.Files;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.constant.DataConstant;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class DataXmlClearCommand extends AbstractCommand {

    private static final Logger logger = LogManager.getLogger(DataXmlClearCommand.class);

    @Override
    public String command() {
        return "data-xml-clear";
    }

    @Override
    public String description() {
        return "Remove XML file.";
    }


    @Override
    public void execute() {
        try {
            System.out.println(
                    "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread
                            .currentThread()
                            .getName());
            final File file = new File(DataConstant.FILE_XML);
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @EventListener(condition = "#event.name eq 'data-xml-clear'")
    public void listener(SimpleEvent event) {
        execute();
    }
}
