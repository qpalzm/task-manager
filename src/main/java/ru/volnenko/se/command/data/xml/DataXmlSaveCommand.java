package ru.volnenko.se.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.File;
import java.nio.file.Files;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.constant.DataConstant;
import ru.volnenko.se.entity.Domain;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class DataXmlSaveCommand extends AbstractCommand {

    private static final Logger logger = LogManager.getLogger(DataXmlSaveCommand.class);

    IDomainService domainService;

    @Autowired
    private void setDomainService(IDomainService domainService) {
        this.domainService = domainService;
    }

    @Override
    public String command() {
        return "data-xml-save";
    }

    @Override
    public String description() {
        return "Save Domain to XML.";
    }


    @Override
    public void execute() {
        try {
            System.out.println(
                    "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread
                            .currentThread()
                            .getName());
            System.out.println("[DATA XML SAVE]");
            final Domain domain = new Domain();
            domainService.export(domain);
            final ObjectMapper objectMapper = new XmlMapper();
            final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
            final String json = objectWriter.writeValueAsString(domain);
            final byte[] data = json.getBytes("UTF_8");
            final File file = new File(DataConstant.FILE_XML);
            Files.write(file.toPath(), data);
            System.out.println("[OK]");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @EventListener(condition = "#event.name eq 'data-xml-save'")
    public void listener(SimpleEvent event) {
        execute();
    }
}
