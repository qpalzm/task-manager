package ru.volnenko.se.command.project;

import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class ProjectCreateCommand extends AbstractCommand {

    IProjectRepository projectRepository;
    private Scanner scanner;

    @Autowired
    private void setProjectRepository(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Autowired
    private void setScanner() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public String command() {
        return "project-create";
    }


    @Override
    public void execute() {
        System.out.println(
                "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread.currentThread()
                        .getName());
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = scanner.nextLine();
        projectRepository.createProject(name);
        System.out.println("[OK]");
        System.out.println();
    }

    @EventListener(condition = "#event.name eq 'project-create'")
    public void listener(SimpleEvent event) {
        execute();
    }

}
