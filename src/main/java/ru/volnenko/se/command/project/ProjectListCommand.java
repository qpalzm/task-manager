package ru.volnenko.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class ProjectListCommand extends AbstractCommand {

    IProjectRepository projectRepository;

    @Autowired
    private void setProjectRepository(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects.";
    }


    @Override
    public void execute() {
        System.out.println(
                "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread.currentThread()
                        .getName());
        System.out.println("[PROJECT LIST]");
        int index = 1;
        for (Project project : projectRepository.getListProject()) {
            System.out.println(index++ + ". " + project.getName());
        }
        System.out.println();
    }

    @EventListener(condition = "#event.name eq 'project-list'")
    public void listener(SimpleEvent event) {
        execute();
    }
}
