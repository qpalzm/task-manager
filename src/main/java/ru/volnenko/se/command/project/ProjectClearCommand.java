package ru.volnenko.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.event.SimpleEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class ProjectClearCommand extends AbstractCommand {

    IProjectRepository projectRepository;

    @Autowired
    private void setProjectRepository(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }


    @Override
    public void execute() {
        System.out.println(
                "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread.currentThread()
                        .getName());
        projectRepository.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    @EventListener(condition = "#event.name eq 'project-clear'")
    public void listener(SimpleEvent event) {
        execute();
    }

}
