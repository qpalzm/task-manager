package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Domain;

/**
 * @author Denis Volnenko
 */
@Component
public final class DomainService implements IDomainService {

    IProjectService projectService;
    ITaskService taskService;

    @Autowired
    private void setProjectService(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Autowired
    private void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) {
            return;
        }
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) {
            return;
        }
        domain.setProjects(projectService.getListProject());
        domain.setTasks(taskService.getListTask());
    }

}
