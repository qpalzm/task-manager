package ru.volnenko.se.service;

import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class ScannerImpl {

    private static final Logger logger = LogManager.getLogger(ScannerImpl.class);

    Scanner scanner;

    private ScannerImpl() {
        this.scanner = new Scanner(System.in);
    }

    public Scanner getScanner() {
        return scanner;
    }


    public String nextLine() {
        try {
            return scanner.nextLine();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return "";
    }
}
