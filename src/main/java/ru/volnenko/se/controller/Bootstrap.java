package ru.volnenko.se.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.error.CommandCorruptException;
import ru.volnenko.se.event.SimpleEvent;
import ru.volnenko.se.service.ScannerImpl;

/**
 * @author Denis Volnenko
 */
@Component
public final class Bootstrap {

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    public Bootstrap(List<AbstractCommand> commandsClass) {
        for (AbstractCommand command : commandsClass) {
            final String cliCommand = command.command();
            final String cliDescription = command.description();
            if (cliCommand == null || cliCommand.isEmpty()) {
                throw new CommandCorruptException();
            }
            if (cliDescription == null || cliDescription.isEmpty()) {
                throw new CommandCorruptException();
            }
            commands.put(cliCommand, command);
        }
    }

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private ScannerImpl scanner;

    public void running() throws Exception {
        start();
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.getScanner().nextLine();
            execute(command);
        }
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) {
            return;
        }
        SimpleEvent event = new SimpleEvent(this);
        event.setName(command);
        publisher.publishEvent(event);
    }

    @Autowired
    private void setScanner(ScannerImpl scanner) {
        this.scanner = scanner;
    }
}
