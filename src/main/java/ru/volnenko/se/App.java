package ru.volnenko.se;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.volnenko.se.config.ApplicationConfig;
import ru.volnenko.se.controller.Bootstrap;


public class App {

    public static void main(String[] args) throws Exception {
        System.out.println(
                "This method run in new thread " + Thread.currentThread().getId() + " name " + Thread.currentThread()
                        .getName());
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.running();
    }

}
