package ru.volnenko.se.event;

import org.springframework.context.ApplicationEvent;

public class SimpleEvent extends ApplicationEvent {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SimpleEvent(Object source) {
        super(source);
    }
}
